/**
 * Created by GentlyGuitar on 3/12/2017.
 */

/* derived class always needs to call super()
 * otherwise, "this" will be undefined reference
 */
class DomObj {
    dom() {
        return this._dom;
    }
}

class Cell extends DomObj {
    constructor(pos) {
        super();
        this._id = GenMaze.posToStr(pos);
        this._dom = $('<td>');
        this._dom.attr("id", this._id);
        this._pos = pos;

        this.initStyle();
    }

    pos() {
        return this._pos;
    }

    id() {
        return this._id;
    }

    initStyle() {
        this._dom.css("width", "18px");
        this._dom.css("height", "18px");
        this._dom.css("transition", "background-color 0.5s");
        this._dom.css("background-color", "#495D63");
        this._dom.css("border", "2px solid black");
        this._dom.attr("class", "tooltip");
        this._dom.attr("title", this._id);
        //newtd.css("-webkit-animation", "spin 4s linear infinite");
        //newtd.css("transform", "rotateY( 10deg )");
    }

    setText(text) {
        this._dom.text(text);
        this._dom.css("text-align", "center");
        this._dom.css("font-size", "13px");
        //this._dom.css("color", "#f73c42");
        //this._dom.css("color", "#404040");
    }

    setColor(color) {
        this._dom.css("color", color);
    }

    setWalker(text) {
        this.setText(text);
        this.setColor("#404040");
    }

    setDestination() {
        this.setText("❤");
        this.setColor("#f73c42");
    }

    removeText() {
        this.setText("");
    }

    removeBorder(dir) {
        this._dom.css("border-"+dir.str(), "0px");
    }

    setBorderColor(dir, color) {
        this._dom.css("border-"+dir.str(), "2px solid "+color);
    }

    animateBorderColor(dir, color) {
        /* not work, it always sets multiple borders */
        let border = "border-"+dir.str();
        let old_value = this._dom.css(border);
        let value = "2px solid "+color;
        this._dom.css({
            border: value,
            transition : 'color 10s ease-in-out',
        });
        // // this._dom.animate({
        // //     border: value
        // // }, 1000, function () {
        // //     this._dom.animate({
        // //         border: value
        // //     }, 1000);
        // // });
        // this._dom.animate({
        //     border: value
        // }, 1000);
    }

    setBGColor(color) {
        this._dom.css("background-color", color);
    }

    setCellBlack() {
        setBGColor("#404040");
    }

}

class GridRow extends DomObj {
    constructor() {
        super();
        this._dom = $('<tr>');
        this._cells = [];
    }

    appendCell(cell) {
        this._dom.append(cell.dom());
        this._cells.push(cell);
    }

    at(i) {
        return this._cells[i];
    }

    do_cell(routine) {
        for (let cell of this._cells) {
            routine(cell);
        }
    }
}

class GridTable extends DomObj {
    constructor() {
        super();
        this._dom = $("<table>");
        this._tbody = $("<tbody>");
        this._dom.append(this._tbody);
        this._dom.css("margin", "50px auto");
        this._dom.css("border", "2px solid black");
        this._dom.css("border-collapse", "collapse");
        this._rows = [];
    }

    appendRow(row) {
        this._tbody.append(row.dom());
        this._rows.push(row);
    }

    at(i, j) {
        return this._rows[i].at(j);
    }

    atPos(p) {
        return this.at(p[0], p[1]);
    }

    do_row(routine) {
        for(let row of this._rows) {
            row.do_cell(routine);
        }
    }
}


class Walker {
    setPos(pos) {
        this._pos = pos;
    }

    pos() {
        return this._pos;
    }

    pos_str() {
        return GenMaze.posToStr(this._pos);
    }

    str() {
        return "●";
    }
}

class Maze {
    constructor(rows, cols) {
        this._rows = rows;
        this._cols = cols;
        this._table = new GridTable();
        this._start_pos = [this._rows/2, this._cols/2];
        this._graph = new Graph();
        this._walker = new Walker();
        this._dest = [0, 0];
        this.init();
    }


    rows() {
        return this._rows;
    }

    cols() {
        return this._cols;
    }

    walker() {
        return this._walker;
    }

    appendToBody() {
        $("body").append(this._table.dom());
    }

    prependToBody() {
        $("body").prepend(this._table.dom());
    }

    remove() {
        this._table.dom().remove();
    }

    redraw() {
        console.log(this._table.dom());
        this._table.dom().remove();
        this._table = new GridTable();
        this.draw();
    }

    removeDirWall(pos, dir) {
        this._table.atPos(pos).removeBorder(dir);
        let nb = this.getNeighborPos(pos, dir);
        this._table.atPos(nb).removeBorder(dir.reverse());
        this.addGraphEdge(GenMaze.posToStr(pos), GenMaze.posToStr(nb));

    }

    setWallPink(pos, dir) {
        let nb = this.getNeighborPos(pos, dir);
        this._table.atPos(pos).setBorderColor(dir, "#ff8080");
        this._table.atPos(nb).setBorderColor(dir.reverse(), "#ff8080");
        // this._table.atPos(pos).animateBorderColor(dir, "#ff8080");
        // this._table.atPos(nb).animateBorderColor(dir.reverse(), "#ff8080");
    }


    init() {
        for (let i = 0; i < this._rows; i++) {
            let row = new GridRow();
            this._table.appendRow(row);

            for (let j = 0; j < this._cols; j++) {
                let cell = new Cell([i, j]);
                row.appendCell(cell);
                this._graph.addVertex(cell.id());
                //this._graph.addVertex(new GraphVertex(cell.pos()));
            }
        }
        this.setWalkerPos(this.startPos());
        //this.moveWalkerTo(this.startPos());
    }

    startPos() {
        return this._start_pos;
    }

    randomStartPos() {
        let r = GenMaze.getRandomInt(0, this.rows()-1);
        let c = GenMaze.getRandomInt(0, this.cols()-1);
        return [r, c];
    }

    setPosWhite(pos) {
        this._table.at(pos[0], pos[1]).setBGColor("#fff");
    }

    setPosPink(pos) {
        this._table.at(pos[0], pos[1]).setBGColor("#ff8080");
        //this._table.at(pos[0], pos[1]).setBGColor("#E5F993");
    }

    getNeighborPos(pos, dir) {
        return [pos[0]+dir.offset()[0], pos[1]+dir.offset()[1]];
    }

    isBorder(pos) {
        let [r, c] = pos;
        return r === this._rows - 1 || c === this._cols - 1;
    }

    isOutOfBounds(pos) {
        let [r, c] = pos;
        return r > this._rows - 1
            || c > this._cols - 1
            || r < 0
            || c < 0;
    }

    isInRange(pos) {
        return !this.isOutOfBounds(pos);
    }

    /* for solving */
    graph() {
        return this._graph;
    }

    dest_str() {
        return GenMaze.posToStr(this._dest);
    }

    setWalkerPos(pos) {
        this._walker.setPos(pos);
    }

    drawWalker() {
        let pos = this._walker.pos();
        let cell = this._table.atPos(pos);
        cell.setWalker(this._walker.str());
    }

    drawDestination() {
        let cell = this._table.atPos(this._dest);
        cell.setDestination();
    }

    addGraphEdge(p1, p2) {
        this._graph.addEdge(p1, p2);
    }

    moveWalkerTo(pos) {
        let old_pos = this._walker.pos();
        let old_cell = this._table.atPos(old_pos);
        old_cell.removeText();
        this.setWalkerPos(pos);
        this.drawWalker();

    }

    dumpGraph() {

        this._graph.dump();
    }


    /* iterator */
    do_cell(routine) {
        this._table.do_row(routine);
    }

    do_pos(routine) {
        for (let i = 0; i < this._rows; i++) {
            for (let j = 0; j < this._cols; j++) {
                routine([i, j]);
            }
        }
    }
}


class Direction {
    constructor(vec) {
        this._vector = vec;
        switch (vec.join(" ")) {
            case "-1 0": {
                this._str = "top";
                break;
            }
            case "1 0": {
                this._str = "bottom";
                break;
            }
            case "0 1": {
                this._str = "right";
                break;
            }
            case "0 -1": {
                this._str = "left";
                break;
            }
        }
    }

    offset() {
        return this._vector;
    }

    str() {
        return this._str;
    }

    id() {
        return this._str;
    }

    reverse() {
        return new Direction([-this._vector[0], -this._vector[1]]);
    }


}

const AuxDirection = {
    "up":    [-1,  0],
    "down": [ 1,  0],
    "right":   [ 0,  1],
    "left":   [ 0, -1],
};




// function setCellText(cell) {
//     cell.text("❤");
//     cell.css("text-align", "center");
//     cell.css("font-size", "13px");
//     cell.css("color", "#f73c42");
// }
//

