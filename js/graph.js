/**
 * Created by GentlyGuitar on 3/14/2017.
 */

class GraphVertex {
    constructor(id) {
        this._id = id;
        //this._marked = false;
    }
}

class Graph {
    constructor() {
        this._nodes = {};
        this._mark = {};
    }

    mark(v) {
        this._mark[v] = 1;
    }

    isMarked(v) {
        return (v in this._mark);
    }

    unmark(v) {
        delete this._mark[v];
    }

    addVertex(v) {
        //console.log("add", v);
        this._nodes[v] = [];
    }

    addEdge(v1, v2) {
        if (this._nodes[v1].indexOf(v2) === -1) {
            this._nodes[v1].push(v2);
        }

        if (this._nodes[v2].indexOf(v1) === -1) {
            this._nodes[v2].push(v1);
        }
        //this.dump();
    }

    getAdjacentList(v) {
        return this._nodes[v];
    }

    dump() {
        for (let k of Object.keys(this._nodes)) {
            console.log("****** edge", k, "*****");
            for (let n of this._nodes[k]) {
                console.log(k, "->", n);
            }
        }
    }
}