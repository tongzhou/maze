/**
 * Created by GentlyGuitar on 3/14/2017.
 */

let FrontierVerbose = false;

class PrimGenMaze extends GenMaze {
    /* return all distinct neighbors of vertices at set */
    /* spt is an array of MazeVertex */
    static getFrontiers(spt) {
        if (FrontierVerbose)
            console.log("spt:", spt.length);

        let raw_set = [];
        let distinct_set = [];
        let aux_key_set = {};
        for (let v of spt) {
            raw_set = raw_set.concat(v.getFrontiers());
            aux_key_set[v.pos_str()] = 1;
            if (FrontierVerbose)
                console.log(v.pos_str());
        }

        if (FrontierVerbose)
            console.log("distinct frontiers:");

        for (let f of raw_set) {
            if (!(f.pos_str() in aux_key_set)) {
                aux_key_set[f.pos_str()] = 1;
                distinct_set.push(f);
                if (FrontierVerbose)
                    console.log(f.pos_str());
            }
        }

        return distinct_set;
    }

    static getFrontiers2(spt) {
        if (FrontierVerbose)
            console.log("spt:", spt.length);

        let results = [];
        let distinct_set = new Set();
        let sptvs = new Set();

        for (let v of spt) {
            sptvs.add(v.pos_str());
        }

        for (let v of GenMaze.shuffleArray(spt)) {
            for (let f of v.getFrontiers()) {
                if (!distinct_set.has(f.pos_str()) && !sptvs.has(f.pos_str())) {
                    results.push(f);
                    distinct_set.add(f.pos_str());
                }
            }
        }

        console.log(distinct_set);
        return results;
    }

    static randomFrontier(spt) {
        let frontiers = PrimGenMaze.getFrontiers(spt);
        return getRandomItem(frontiers);
    }

    static initVertices(maze) {
        PrimGenMaze.vertices = [];
        for (let i = 0; i < maze.rows(); i++) {
            let rs = [];
            for (let j = 0; j < maze.cols(); j++) {
                rs.push(new MazeVertex([i, j]));
            }
            PrimGenMaze.vertices.push(rs);
        }
    }

    static getVertexAtPos(pos) {
        if (pos[0] > PrimGenMaze.vertices.length
            || pos[1] > PrimGenMaze.vertices[0].length) {
            throw "index vertices out of bounds";
        }
        return PrimGenMaze.vertices[pos[0]][pos[1]];
    }

    static initFrontiers() {
        // PrimGenMaze.frontiers = {};
        // for (let i = 0; i < maze.rows(); i++) {
        //     for (let j = 0; j < maze.cols(); j++) {
        //         let v = PrimGenMaze.getVertexAtPos(i, j);
        //         PrimGenMaze.frontiers[v] = -1;
        //     }
        // }
        PrimGenMaze.frontiers = [];
        PrimGenMaze.vfrontiers = new Set();
        PrimGenMaze.tree = new Set();
    }

    static expandFrontiers(maze, newv) {
        PrimGenMaze.tree.add(newv);
        //PrimGenMaze.vfrontiers.delete(newv);
        PrimGenMaze.frontiers = PrimGenMaze.frontiers.filter(item => !PrimGenMaze.tree.has(PrimGenMaze.getVertexAtPos(item.pos())));

        for (let n of newv.getFrontiers()) {
            let pos = n.pos();
            if (maze.isInRange(pos)) {
                let v = PrimGenMaze.getVertexAtPos(pos);
                console.log("is in tree:", v, PrimGenMaze.tree.has(v));
                if (!PrimGenMaze.tree.has(v)) {
                    // if (!PrimGenMaze.vfrontiers.has(v)) {
                    //     PrimGenMaze.vfrontiers.add(v);
                    // }
                    PrimGenMaze.frontiers.push(n);
                }
            }
        }


        console.log("new", newv.pos());
        console.log(PrimGenMaze.tree);

    }

    static run(maze) {
        PrimGenMaze.initVertices(maze);
        PrimGenMaze.initFrontiers();
        let init_pos = maze.randomStartPos();
        console.log("start v:", init_pos);
        let init_vertex = new MazeVertex(init_pos);
        let spt = [];

        spt.push(init_vertex);
        PrimGenMaze.expandFrontiers(maze, init_vertex);
        console.log(PrimGenMaze.frontiers);

        if (MazeSystem.GUI)
            maze.setPosWhite(init_pos);
        let vertex_num = PrimGenMaze.vertices.length*PrimGenMaze.vertices[0].length;

        let steps = 0;
        let timer0 = performance.now();
        function stepIt() {
            if (spt.length <= vertex_num) {
                let frontiers = PrimGenMaze.frontiers;
                for (let f of frontiers) {
                    if (maze.isInRange(f.pos())) {
                        if (MazeSystem.GUI)
                            maze.setPosPink(f.pos());
                    }
                }
                let good = false;
                do {
                    let frontier = getRandomItem(frontiers);
                    console.log("select frontier:", frontier.pos_str());
                    if (maze.isInRange(frontier.pos())) {
                        let next_v = PrimGenMaze.getVertexAtPos(frontier.pos());
                        if (spt.indexOf(next_v) === -1) {
                            good = true;
                            spt.push(next_v);
                            PrimGenMaze.expandFrontiers(maze, next_v);
                            if (MazeSystem.GUI) {
                                maze.setPosWhite(frontier.pos());
                                maze.removeDirWall(frontier.pos(), frontier.dir());
                            }
                        }
                    }
                    frontiers = frontiers.filter(item => item !== frontier);
                } while (!good);

                steps++;
                if (MazeSystem.GUI) {
                    if (MazeSystem.curgen === MazeSystem.gens.prim)
                        setTimeout(stepIt, MazeSystem.speed);
                }
                else {
                    stepIt();
                }
            }
            else {
                steps++;
                let timer1 = performance.now();
                let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                MazeSystem.addTimer(steps, elapsed);
            }

        }
        stepIt();
    }

    static old_run(maze) {
        PrimGenMaze.initVertices(maze);
        let init_pos = maze.randomStartPos();
        let init_vertex = new MazeVertex(init_pos);
        let spt = [];
        let aux_vertex_set = {};

        spt.push(init_vertex);
        aux_vertex_set[init_vertex.pos_str()] = 1;

        if (MazeSystem.GUI)
            maze.setPosWhite(init_pos);
        let vertex_num = PrimGenMaze.vertices.length*PrimGenMaze.vertices[0].length;

        let steps = 0;
        let timer0 = performance.now();
        function stepIt() {
            if (spt.length < vertex_num) {
                let frontiers = PrimGenMaze.getFrontiers(spt);
                for (let f of frontiers) {
                    if (maze.isInRange(f.pos())) {
                        if (MazeSystem.GUI)
                            maze.setPosPink(f.pos());
                    }
                }
                let good = false;
                do {
                    let frontier = getRandomItem(frontiers);
                    console.log("select frontier:", frontier.pos_str());
                    if (maze.isInRange(frontier.pos())) {
                        let next_v = PrimGenMaze.getVertexAtPos(frontier.pos());
                        if (spt.indexOf(next_v) === -1) {
                            good = true;
                            spt.push(next_v);
                            if (MazeSystem.GUI) {
                                maze.setPosWhite(frontier.pos());
                                maze.removeDirWall(frontier.pos(), frontier.dir());
                            }
                        }
                    }
                    frontiers = frontiers.filter(item => item !== frontier);
                } while (!good);

                steps++;
                if (MazeSystem.GUI) {
                    if (MazeSystem.curgen === MazeSystem.gens.prim)
                        setTimeout(stepIt, MazeSystem.speed);
                }
                else {
                    stepIt();
                }
            }
            else {
                steps++;
                let timer1 = performance.now();
                let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                MazeSystem.addTimer(steps, elapsed);
            }

        }
        stepIt();
    }
}