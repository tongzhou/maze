/**
 * Created by tzhou on 4/8/17.
 */

class ABGenMaze extends GenMaze {

    static initVertices(maze) {
        ABGenMaze.vertices = [];
        for (let i = 0; i < maze.rows(); i++) {
            let rs = [];
            for (let j = 0; j < maze.cols(); j++) {
                rs.push(new MazeVertex([i, j]));
            }
            ABGenMaze.vertices.push(rs);
        }
    }

    static getVertexAtPos(pos) {
        if (pos[0] > ABGenMaze.vertices.length
            || pos[1] > ABGenMaze.vertices[0].length) {
            throw "index vertices out of bounds";
        }
        return ABGenMaze.vertices[pos[0]][pos[1]];
    }

    static run(maze) {
        ABGenMaze.initVertices(maze);
        let init_pos = maze.randomStartPos();
        let init_vertex = ABGenMaze.getVertexAtPos(init_pos);
        //let spt = [];

        let cur_v = init_vertex;
        let vertex_set = new Set();
        vertex_set.add(cur_v);
        console.log("add ", cur_v.id());
        let steps = 0;


        let timer0 = performance.now();

        if (MazeSystem.GUI) {
            function stepIt() {
                console.log("cur vertex:", cur_v.id());
                if (vertex_set.size < maze.rows() * maze.cols()) {
                    let neighbors = cur_v.getLegitNeighbors(maze.rows());
                    let [random_neighbor_pos, dir] = GenMaze.getRandomItem(neighbors);
                    console.log(random_neighbor_pos);
                    let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                    if (!vertex_set.has(neighbor)) {
                        if (MazeSystem.GUI) {
                            maze.removeDirWall(random_neighbor_pos, dir);
                        }
                        vertex_set.add(neighbor);
                    }

                    if (MazeSystem.GUI) {
                        maze.setPosWhite(cur_v.pos());
                        maze.setPosPink(neighbor.pos());
                    }

                    cur_v = neighbor;

                    if (vertex_set.size === maze.rows() * maze.cols()) {
                        if (MazeSystem.GUI)
                            maze.setPosWhite(cur_v.pos());
                    }

                    if (MazeSystem.curgen === MazeSystem.gens.ab) {
                        setTimeout(stepIt, MazeSystem.speed);
                    }
                    //stepIt();
                    steps++;
                }
                else {
                    /* exit */
                    steps++;
                    let timer1 = performance.now();
                    let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                    MazeSystem.addTimer(steps, elapsed);
                }
            }

            stepIt();
        }
        else {
            while (vertex_set.size < maze.rows() * maze.cols()) {
                let neighbors = cur_v.getLegitNeighbors(maze.rows());
                let [random_neighbor_pos, dir] = GenMaze.getRandomItem(neighbors);
                console.log(random_neighbor_pos);
                let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                if (!vertex_set.has(neighbor)) {
                    if (MazeSystem.GUI) {
                        maze.removeDirWall(random_neighbor_pos, dir);
                    }
                    vertex_set.add(neighbor);
                }

                if (MazeSystem.GUI) {
                    maze.setPosWhite(cur_v.pos());
                    maze.setPosPink(neighbor.pos());
                }

                cur_v = neighbor;

                steps++;
                if (vertex_set.size === maze.rows() * maze.cols()) {
                    if (MazeSystem.GUI)
                        maze.setPosWhite(cur_v.pos());
                }
            }
            let timer1 = performance.now();
            let elapsed = ((timer1 - timer0)/1000).toFixed(5);
            MazeSystem.addTimer(steps, elapsed);
        }
    }

}