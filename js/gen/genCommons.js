/**
 * Created by GentlyGuitar on 3/14/2017.
 */

class GenMaze {
    /**
     * Randomize array element order in-place.
     * Using Durstenfeld shuffle algorithm.
     */
    static shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    /* Returns a random integer between min (inclusive) and max (inclusive) */
    static getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static getRandomItem(items) {
        let item = items[Math.floor(Math.random()*items.length)];
        return item;
    }

    static randomProperty(obj) {
        let keys = Object.keys(obj);
        //return obj[keys[ keys.length * Math.random() << 0]];
        let key = keys[GenMaze.getRandomInt(0, keys.length-1)];
        //return obj[key];
        return key;
    }

    static randomDirection() {
        let keys = Object.keys(AuxDirection);
        //return obj[keys[ keys.length * Math.random() << 0]];
        let key = keys[GenMaze.getRandomInt(0, keys.length-1)];
        //return obj[key];
        return new Direction(AuxDirection[key]);
    }

    static randomDirectionExcept(blacklist) {
        let dir;
        let lguard = 0;
        do {
            dir = GenMaze.randomDirection();
            lguard++;
        } while (blacklist.indexOf(dir.id()) > -1 && lguard < 100);

        return dir;
    }

    static arrayRemove(arr, value) {
        return arr.filter(item => item !== value);
    }

    static posToStr(pos) {
        return pos.join("-");
    }

    static strToPos(str) {
        return str.split("-");
    }
}

class Frontier {
    constructor(pos, dir) {
        this._pos = pos;
        this._dir = dir;
    }

    pos_str() {
        return this._pos.join("-");
    }

    pos() {
        return this._pos;
    }

    dir() {
        return this._dir;
    }
}

class MazeVertex {
    constructor(pos) {
        this._parent = this; // used for disjoint set
        this._pos = pos;
        this._id = pos.join("-");
    }

    id() {
        return this._id;
    }

    pos_str() {
        return this._pos.join("-");
    }

    pos() {
        return this._pos;
    }

    parent() {
        return this._parent;
    }

    setParent(p) {
        this._parent = p;
    }

    /* no boundary check */
    getNeighbors() {
        let nb = [];
        let [r, c] = this._pos;
        nb.push([r-1, c]);
        nb.push([r+1, c]);
        nb.push([r, c-1]);
        nb.push([r, c+1]);
        return nb;
    }

    /* has boundary check */
    getLegitNeighbors(dim) {
        let nb = [];
        let [r, c] = this._pos;
        if (r-1 >= 0) {
            nb.push([[r-1, c], new Direction(AuxDirection.down)]);
        }
        if (r+1 < dim) {
            nb.push([[r+1, c], new Direction(AuxDirection.up)]);
        }
        if (c-1 >= 0) {
            nb.push([[r, c-1], new Direction(AuxDirection.right)]);
        }
        if (c+1 < dim) {
            nb.push([[r, c+1], new Direction(AuxDirection.left)]);
        }
        return nb;
    }

    /* has boundary check */
    getRawLegitNeighbors(dim) {
        let nb = [];
        let [r, c] = this._pos;
        if (r-1 >= 0) {
            nb.push([r-1, c]);
        }
        if (r+1 < dim) {
            nb.push([r+1, c]);
        }
        if (c-1 >= 0) {
            nb.push([r, c-1]);
        }
        if (c+1 < dim) {
            nb.push([r, c+1]);
        }
        return nb;
    }

    /* no boundary check */
    /* a frontier has a pointer to the closure */
    getFrontiers() {
        let nb = [];
        let [r, c] = this._pos;
        nb.push(new Frontier([r-1, c], new Direction(AuxDirection.down)));
        nb.push(new Frontier([r+1, c], new Direction(AuxDirection.up)));
        nb.push(new Frontier([r, c-1], new Direction(AuxDirection.right)));
        nb.push(new Frontier([r, c+1], new Direction(AuxDirection.left)));
        return nb;
    }

}

function getRandomItem(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

class MazeEdge {
    constructor(pos, dir) {
        this._pos = pos;
        this._dir = dir;

    }

    str() {
        return this._pos.join("-");
    }

    dir() {
        return this._dir;
    }

    posAndDir() {
        let [row, col] = this._pos;
        if (this._dir == "horizontal") {
            return [[row, Math.floor(col)], new Direction(AuxDirection.right)];
        }

        if (this._dir == "vertical") {
            return [[Math.floor(row), col], new Direction(AuxDirection.down)];
        }

    }

    vertices() {
        let [row, col] = this._pos;
        if (this._dir == "horizontal") {
            // let v1 = new MazeVertex([row, Math.floor(col)]);
            // let v2 = new MazeVertex([row, Math.ceil(col)]);
            // return [v1, v2];
            return [[row, Math.floor(col)], [row, Math.ceil(col)]];
        }

        if (this._dir == "vertical") {
            // let v1 = new MazeVertex([Math.floor(row), col]);
            // let v2 = new MazeVertex([Math.ceil(row), col]);
            // return [v1, v2];
            return [[Math.floor(row), col], [Math.ceil(row), col]];
        }

        throw "invalid edge position";
    }
}
