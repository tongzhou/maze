/**
 * Created by GentlyGuitar on 3/13/2017.
 */

class BackTrackGenMaze extends GenMaze {

    static run(maze) {
        let visited = [];
        let past_dirs = [GenMaze.randomDirection()];
        let start = maze.randomStartPos();
        let terminate = false;
        let steps = 0;
        let cur = start;
        let timer0 = performance.now();

        if (MazeSystem.GUI) {
            function stepIt() {
                console.log("cur:", cur);
                if (!terminate) {
                    visited[cur] = 1;
                    let backdir = past_dirs[past_dirs.length - 1].reverse();
                    let bad_dirs = [backdir.id()];
                    let dir, next;
                    let backtrace = false;
                    let retry = true;
                    do {
                        if (bad_dirs.length === Object.keys(AuxDirection).length) {
                            backtrace = true;
                            break;
                        }
                        dir = GenMaze.randomDirectionExcept(bad_dirs);
                        next = maze.getNeighborPos(cur, dir);
                        if (!maze.isOutOfBounds(next) && !visited[next]) {
                            retry = false;
                        }
                        bad_dirs.push(dir.id());
                    } while (retry);

                    if (backtrace) {
                        console.log("backtrace", backdir.str());
                        if (MazeSystem.GUI)
                            maze.setPosWhite(cur);
                        past_dirs.pop();
                        cur = maze.getNeighborPos(cur, backdir);
                    }
                    else {
                        console.log("move", dir.str());
                        console.log("remove wall", cur[0], cur[1], dir);
                        if (MazeSystem.GUI) {
                            maze.setPosPink(cur);
                            maze.removeDirWall(cur, dir);
                        }

                        past_dirs.push(dir);
                        cur = next;
                    }

                    console.log("step", steps, "done");
                    steps++;

                    if (cur[0] == start[0] && cur[1] == start[1] || steps > 10000000) {
                        terminate = true;
                    }

                    if (MazeSystem.curgen === MazeSystem.gens.backtrack)
                        setTimeout(stepIt, MazeSystem.speed);
                }
                else {
                    if (MazeSystem.GUI)
                        maze.setPosWhite(start);

                    /* exit */
                    steps++;
                    let timer1 = performance.now();
                    let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                    MazeSystem.addTimer(steps, elapsed);

                }

            }
            stepIt();
        }
        else {
            while (!terminate) {
                visited[cur] = 1;
                let backdir = past_dirs[past_dirs.length - 1].reverse();
                let bad_dirs = [backdir.id()];
                let dir, next;
                let backtrace = false;
                let retry = true;
                do {
                    if (bad_dirs.length === Object.keys(AuxDirection).length) {
                        backtrace = true;
                        break;
                    }
                    dir = GenMaze.randomDirectionExcept(bad_dirs);
                    next = maze.getNeighborPos(cur, dir);
                    if (!maze.isOutOfBounds(next) && !visited[next]) {
                        retry = false;
                    }
                    bad_dirs.push(dir.id());
                } while (retry);

                if (backtrace) {
                    console.log("backtrace", backdir.str());
                    if (MazeSystem.GUI)
                        maze.setPosWhite(cur);
                    past_dirs.pop();
                    cur = maze.getNeighborPos(cur, backdir);
                }
                else {
                    console.log("move", dir.str());
                    console.log("remove wall", cur[0], cur[1], dir);
                    if (MazeSystem.GUI) {
                        maze.setPosPink(cur);
                        maze.removeDirWall(cur, dir);
                    }

                    past_dirs.push(dir);
                    cur = next;
                }

                console.log("step", steps, "done");
                steps++;

                if (cur[0] == start[0] && cur[1] == start[1] || steps > 10000000) {
                    terminate = true;
                }
            }
            /* exit */
            let timer1 = performance.now();
            let elapsed = ((timer1 - timer0)/1000).toFixed(5);
            MazeSystem.addTimer(steps, elapsed);
        }

    }

}

