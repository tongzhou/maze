/**
 * Created by GentlyGuitar on 3/13/2017.
 */

class UnionFind {
    inSameSet(items) {

    }

    static hasSameRoot(x, y) {
        let xr = UnionFind.find(x);
        let yr = UnionFind.find(y);
        return xr === yr; // literally compare reference
    }

    // static findInner(node) {
    //     if (node.parent() === node) {
    //         return node;
    //     }
    //     else {
    //         return UnionFind.find(node.parent());
    //     }
    // }

    static find(node) {
        let p = node;
        while (p.parent() != p) {
            p = p.parent();
        }
        return p;
    }

    static union(x, y) {
        let xr = UnionFind.find(x);
        let yr = UnionFind.find(y);
        if (Math.random() > 0.5) {
            xr.setParent(yr);
        }
        else {
            yr.setParent(xr);
        }
    }

    static test_() {
        let x = new MazeVertex([0, 0]);
        let y = new MazeVertex([0, 1]);
        UnionFind.union(x, y);
        console.log(UnionFind.hasSameRoot(x, y));
    }
}



class KruskalGenMaze extends GenMaze {
    static initVertices(maze) {
        KruskalGenMaze.vertices = [];
        for (let i = 0; i < maze.rows(); i++) {
            let rs = [];
            for (let j = 0; j < maze.cols(); j++) {
                rs.push(new MazeVertex([i, j]));
            }
            KruskalGenMaze.vertices.push(rs);
        }
    }

    static getAllEdgesShuffled(maze) {
        let init_offset = 0.5;
        let edges = [];

        for (let i = 0; i < maze.rows(); i++) {
            for (let j = init_offset; j < maze.cols()-1; j++) {
                edges.push(new MazeEdge([i, j], "horizontal"));
                edges.push(new MazeEdge([j, i], "vertical")); // transpose, only works for square maze
            }
        }

        return KruskalGenMaze.shuffleArray(edges);
    }

    static getVertexAtPos(pos) {
        if (pos[0] > KruskalGenMaze.vertices.length
            || pos[1] > KruskalGenMaze.vertices[0].length) {
            throw "index vertices out of bounds";
        }
        return KruskalGenMaze.vertices[pos[0]][pos[1]];
    }

    static run(maze) {
        KruskalGenMaze.initVertices(maze);
        let edges = KruskalGenMaze.getAllEdgesShuffled(maze);
        let i = 0;
        let sptvs = new Set();
        let steps = 0;

        let timer0 = performance.now();

        if (MazeSystem.GUI) {
            function stepIt() {
                if (sptvs.size < maze.rows()*maze.cols()) {
                    let edge = edges[i];
                    let [pos, dir] = edge.posAndDir();
                    if (MazeSystem.GUI) {
                        maze.setWallPink(pos, dir);
                    }

                    console.log("edge:", edge.str());
                    let vs = edge.vertices();
                    let v1 = KruskalGenMaze.getVertexAtPos(vs[0]);
                    let v2 = KruskalGenMaze.getVertexAtPos(vs[1]);
                    if (!sptvs.has(v1)) {
                        sptvs.add(v1);
                    }

                    if (!sptvs.has(v2)) {
                        sptvs.add(v2);
                    }

                    if (!UnionFind.hasSameRoot(v1, v2)) {
                        UnionFind.union(v1, v2);
                        console.log("remove wall", pos, dir);
                        if (MazeSystem.GUI) {
                            maze.removeDirWall(pos, dir);
                            maze.setPosWhite(pos);
                            maze.setPosWhite(maze.getNeighborPos(pos, dir));
                        }
                    }
                    i++;

                    steps++;
                    if (MazeSystem.GUI) {
                        if (MazeSystem.curgen === MazeSystem.gens.kruskal)
                            setTimeout(stepIt, MazeSystem.speed);
                    }
                    else {
                        stepIt();
                    }
                }
                else {
                    steps++;
                    let timer1 = performance.now();
                    let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                    MazeSystem.addTimer(steps, elapsed);
                }
            }
            stepIt();
        }
        else {
            /* for timeing */
            while (sptvs.size < maze.rows()*maze.cols()) {
                let edge = edges[i];
                let [pos, dir] = edge.posAndDir();
                if (MazeSystem.GUI) {
                    maze.setWallPink(pos, dir);
                }

                console.log("edge:", edge.str());
                let vs = edge.vertices();
                let v1 = KruskalGenMaze.getVertexAtPos(vs[0]);
                let v2 = KruskalGenMaze.getVertexAtPos(vs[1]);
                if (!sptvs.has(v1)) {
                    sptvs.add(v1);
                }

                if (!sptvs.has(v2)) {
                    sptvs.add(v2);
                }

                if (!UnionFind.hasSameRoot(v1, v2)) {
                    UnionFind.union(v1, v2);
                    console.log("remove wall", pos, dir);
                    if (MazeSystem.GUI) {
                        maze.removeDirWall(pos, dir);
                        maze.setPosWhite(pos);
                        maze.setPosWhite(maze.getNeighborPos(pos, dir));
                    }
                }
                i++;

                steps++;
            }

            steps++;
            let timer1 = performance.now();
            let elapsed = ((timer1 - timer0)/1000).toFixed(5);
            MazeSystem.addTimer(steps, elapsed);

        }
    }
}