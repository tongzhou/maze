/**
 * Created by root on 4/8/17.
 */


class TreeGenMaze extends GenMaze {
    static initVertices(maze) {
        ABGenMaze.vertices = [];
        for (let i = 0; i < maze.rows(); i++) {
            let rs = [];
            for (let j = 0; j < maze.cols(); j++) {
                rs.push(new MazeVertex([i, j]));
            }
            ABGenMaze.vertices.push(rs);
        }
    }

    static getVertexAtPos(pos) {
        if (pos[0] > ABGenMaze.vertices.length
            || pos[1] > ABGenMaze.vertices[0].length) {
            throw "index vertices out of bounds";
        }
        return ABGenMaze.vertices[pos[0]][pos[1]];
    }

    static run_mixed(maze) {
        ABGenMaze.initVertices(maze);
        let init_pos = maze.randomStartPos();
        let init_vertex = ABGenMaze.getVertexAtPos(init_pos);
        //let spt = [];

        let gray = [init_vertex];
        if (MazeSystem.GUI) {
            maze.setPosPink(init_vertex.pos());
        }
        let black = [];
        let step = 0;
        console.log("add", init_vertex.id());
        let timer0 = performance.now();

        if (MazeSystem.GUI) {
            function stepIt() {
                if (gray.length !== 0 && step < 6000000000) {
                    let v;
                    if (Math.random() > 0.5)
                        v = GenMaze.getRandomItem(gray);
                    else
                        v = gray[gray.length-1];

                    let neighbors = v.getLegitNeighbors(maze.rows());
                    /* check if all neighbors are visited */
                    let allvisited = true;
                    let unvisited = [];
                    for (let i = 0; i < neighbors.length; i++) {
                        let [ni, dir] = neighbors[i];
                        let vi = ABGenMaze.getVertexAtPos(ni);
                        if (!gray.includes(vi) && !black.includes(vi)) {
                            allvisited = false;
                            unvisited.push([ni, dir]);
                        }
                    }
                    if (allvisited) {
                        gray = GenMaze.arrayRemove(gray, v);
                        black.push(v);
                        console.log("remove", v.id());
                        if (MazeSystem.GUI) {
                            maze.setPosWhite(v.pos());
                        }
                    }
                    else {
                        let [random_neighbor_pos, dir] = GenMaze.getRandomItem(unvisited);
                        console.log("add ", random_neighbor_pos);
                        let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                        gray.push(neighbor);
                        if (MazeSystem.GUI) {
                            maze.setPosPink(neighbor.pos());
                            maze.removeDirWall(random_neighbor_pos, dir);
                        }
                    }

                    if (MazeSystem.curgen === MazeSystem.gens.tree_mixed) {
                        setTimeout(stepIt, MazeSystem.speed);
                    }
                    step++;
                }
                else {
                    /* exit */
                    step++;
                    let timer1 = performance.now();
                    let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                    MazeSystem.addTimer(step, elapsed);
                }
            }

            stepIt();
        }
        else {
            /* for timing */
            while (gray.length !== 0 && step < 6000000000) {
                let v = GenMaze.getRandomItem(gray);
                if (MazeSystem.GUI) {
                    maze.setPosPink(v.pos());
                }
                let neighbors = v.getLegitNeighbors(maze.rows());
                /* check if all neighbors are visited */
                let allvisited = true;
                let unvisited = [];
                for (let i = 0; i < neighbors.length; i++) {
                    let [ni, dir] = neighbors[i];
                    let vi = ABGenMaze.getVertexAtPos(ni);
                    if (!gray.includes(vi) && !black.includes(vi)) {
                        allvisited = false;
                        unvisited.push([ni, dir]);
                    }
                }
                if (allvisited) {
                    gray = GenMaze.arrayRemove(gray, v);
                    black.push(v);
                    console.log("remove", v.id());
                    if (MazeSystem.GUI) {
                        maze.setPosWhite(v.pos());
                    }
                }
                else {
                    let [random_neighbor_pos, dir] = GenMaze.getRandomItem(unvisited);
                    console.log("add ", random_neighbor_pos);
                    let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                    gray.push(neighbor);

                    if (MazeSystem.GUI) {
                        maze.removeDirWall(random_neighbor_pos, dir);
                    }
                }

                step++;
            }
            /* exit */
            let timer1 = performance.now();
            let elapsed = ((timer1 - timer0)/1000).toFixed(5);
            MazeSystem.addTimer(step, elapsed);
        }
    }


    static run_random(maze) {
        ABGenMaze.initVertices(maze);
        let init_pos = maze.randomStartPos();
        let init_vertex = ABGenMaze.getVertexAtPos(init_pos);
        //let spt = [];

        let gray = [init_vertex];
        if (MazeSystem.GUI) {
            maze.setPosPink(init_vertex.pos());
        }
        let black = [];
        let step = 0;
        console.log("add", init_vertex.id());
        let timer0 = performance.now();

        if (MazeSystem.GUI) {
            function stepIt() {
                if (gray.length !== 0 && step < 6000000000) {
                    let v = GenMaze.getRandomItem(gray);

                    let neighbors = v.getLegitNeighbors(maze.rows());
                    /* check if all neighbors are visited */
                    let allvisited = true;
                    let unvisited = [];
                    for (let i = 0; i < neighbors.length; i++) {
                        let [ni, dir] = neighbors[i];
                        let vi = ABGenMaze.getVertexAtPos(ni);
                        if (!gray.includes(vi) && !black.includes(vi)) {
                            allvisited = false;
                            unvisited.push([ni, dir]);
                        }
                    }
                    if (allvisited) {
                        gray = GenMaze.arrayRemove(gray, v);
                        black.push(v);
                        console.log("remove", v.id());
                        if (MazeSystem.GUI) {
                            maze.setPosWhite(v.pos());
                        }
                    }
                    else {
                        let [random_neighbor_pos, dir] = GenMaze.getRandomItem(unvisited);
                        console.log("add ", random_neighbor_pos);
                        let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                        gray.push(neighbor);
                        if (MazeSystem.GUI) {
                            maze.setPosPink(neighbor.pos());
                            maze.removeDirWall(random_neighbor_pos, dir);
                        }
                    }

                    if (MazeSystem.curgen === MazeSystem.gens.tree_random) {
                        setTimeout(stepIt, MazeSystem.speed);
                    }
                    step++;
                }
                else {
                    /* exit */
                    step++;
                    let timer1 = performance.now();
                    let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                    MazeSystem.addTimer(step, elapsed);
                }
            }

            stepIt();
        }
        else {
            /* for timing */
            while (gray.length !== 0 && step < 6000000000) {
                let v = GenMaze.getRandomItem(gray);
                if (MazeSystem.GUI) {
                    maze.setPosPink(v.pos());
                }
                let neighbors = v.getLegitNeighbors(maze.rows());
                /* check if all neighbors are visited */
                let allvisited = true;
                let unvisited = [];
                for (let i = 0; i < neighbors.length; i++) {
                    let [ni, dir] = neighbors[i];
                    let vi = ABGenMaze.getVertexAtPos(ni);
                    if (!gray.includes(vi) && !black.includes(vi)) {
                        allvisited = false;
                        unvisited.push([ni, dir]);
                    }
                }
                if (allvisited) {
                    gray = GenMaze.arrayRemove(gray, v);
                    black.push(v);
                    console.log("remove", v.id());
                    if (MazeSystem.GUI) {
                        maze.setPosWhite(v.pos());
                    }
                }
                else {
                    let [random_neighbor_pos, dir] = GenMaze.getRandomItem(unvisited);
                    console.log("add ", random_neighbor_pos);
                    let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                    gray.push(neighbor);

                    if (MazeSystem.GUI) {
                        maze.removeDirWall(random_neighbor_pos, dir);
                    }
                }

                step++;
            }
            /* exit */
            let timer1 = performance.now();
            let elapsed = ((timer1 - timer0)/1000).toFixed(5);
            MazeSystem.addTimer(step, elapsed);
        }
    }



    static run_newest(maze) {
        ABGenMaze.initVertices(maze);
        let init_pos = maze.randomStartPos();
        let init_vertex = ABGenMaze.getVertexAtPos(init_pos);
        //let spt = [];

        let gray = [init_vertex];
        let black = [];
        let step = 0;
        console.log("add", init_vertex.id());
        let timer0 = performance.now();

        if (MazeSystem.GUI) {
            function stepIt() {
                if (gray.length !== 0 && step < 6000000000) {
                    let v = gray[gray.length-1];
                    if (MazeSystem.GUI) {
                        maze.setPosPink(v.pos());
                    }
                    let neighbors = v.getLegitNeighbors(maze.rows());
                    /* check if all neighbors are visited */
                    let allvisited = true;
                    let unvisited = [];
                    for (let i = 0; i < neighbors.length; i++) {
                        let [ni, dir] = neighbors[i];
                        let vi = ABGenMaze.getVertexAtPos(ni);
                        if (!gray.includes(vi) && !black.includes(vi)) {
                            allvisited = false;
                            unvisited.push([ni, dir]);
                        }
                    }
                    if (allvisited) {
                        gray = GenMaze.arrayRemove(gray, v);
                        black.push(v);
                        console.log("remove", v.id());
                        if (MazeSystem.GUI) {
                            maze.setPosWhite(v.pos());
                        }
                    }
                    else {
                        let [random_neighbor_pos, dir] = GenMaze.getRandomItem(unvisited);
                        console.log("add ", random_neighbor_pos);
                        let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                        gray.push(neighbor);

                        if (MazeSystem.GUI) {
                            maze.removeDirWall(random_neighbor_pos, dir);
                        }
                    }

                    if (MazeSystem.curgen === MazeSystem.gens.tree_newest) {
                        setTimeout(stepIt, MazeSystem.speed);
                    }
                    step++;
                }
                else {
                    /* exit */
                    step++;
                    let timer1 = performance.now();
                    let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                    MazeSystem.addTimer(step, elapsed);
                }
            }

            stepIt();
        }
        else {
            /* for timing */
            while (gray.length !== 0 && step < 6000000000) {
                let v = gray[gray.length-1];
                if (MazeSystem.GUI) {
                    maze.setPosPink(v.pos());
                }
                let neighbors = v.getLegitNeighbors(maze.rows());
                /* check if all neighbors are visited */
                let allvisited = true;
                let unvisited = [];
                for (let i = 0; i < neighbors.length; i++) {
                    let [ni, dir] = neighbors[i];
                    let vi = ABGenMaze.getVertexAtPos(ni);
                    if (!gray.includes(vi) && !black.includes(vi)) {
                        allvisited = false;
                        unvisited.push([ni, dir]);
                    }
                }
                if (allvisited) {
                    gray = GenMaze.arrayRemove(gray, v);
                    black.push(v);
                    console.log("remove", v.id());
                    if (MazeSystem.GUI) {
                        maze.setPosWhite(v.pos());
                    }
                }
                else {
                    let [random_neighbor_pos, dir] = GenMaze.getRandomItem(unvisited);
                    console.log("add ", random_neighbor_pos);
                    let neighbor = ABGenMaze.getVertexAtPos(random_neighbor_pos);
                    gray.push(neighbor);

                    if (MazeSystem.GUI) {
                        maze.removeDirWall(random_neighbor_pos, dir);
                    }
                }

                step++;
            }
            /* exit */

            let timer1 = performance.now();
            let elapsed = ((timer1 - timer0)/1000).toFixed(5);
            MazeSystem.addTimer(step, elapsed);
        }

    }

}