/**
 * Created by root on 4/8/17.
 */



class EllerGenMaze extends GenMaze {
    static initVertices(maze) {
        EllerGenMaze.vertices = [];
        for (let i = 0; i < maze.rows(); i++) {
            let rs = [];
            for (let j = 0; j < maze.cols(); j++) {
                rs.push(new MazeVertex([i, j]));
            }
            EllerGenMaze.vertices.push(rs);
        }
    }


    static getVertexAtPos(pos) {
        if (pos[0] > EllerGenMaze.vertices.length
            || pos[1] > EllerGenMaze.vertices[0].length) {
            throw "index vertices out of bounds";
        }
        return EllerGenMaze.vertices[pos[0]][pos[1]];
    }


    static run(maze) {
        EllerGenMaze.initVertices(maze);
        //let edges = EllerGenMaze.getAllEdgesShuffled(maze);
        //let i = 0;
        let terminate = false;
        let r = 0;
        let c = 0; /* try dim/2 times union for each row */
        let steps = 0;

        let timer0 = performance.now();
        function stepIt() {
            if (r < maze.rows() && !terminate) {
                if (r === maze.rows()) {

                }

                let v1 = EllerGenMaze.getVertexAtPos([r, c]);
                let v2 = EllerGenMaze.getVertexAtPos([r, c+1]);

                if (!UnionFind.hasSameRoot(v1, v2)) {
                    let doit = false;
                    if (r != maze.rows()-1) {
                        if (Math.random() > 0.5) {
                            doit = true;
                        }
                    }
                    else {
                        doit = true;
                    }

                    if (doit) {
                        UnionFind.union(v1, v2);
                        console.log("remove wall", [r, c], "right");
                        if (MazeSystem.GUI)
                            maze.removeDirWall([r, c], new Direction(AuxDirection.right));
                    }

                }

                if (MazeSystem.GUI) {
                    maze.setPosWhite([r, c]);
                    maze.setPosWhite([r, c+1]);
                }

                c++;

                if (c === maze.cols()-1) {
                    if (MazeSystem.GUI) {
                        maze.setPosWhite([r, maze.cols() - 1]);
                    }

                    if (r != maze.rows()-1) {
                        let root_map = {};
                        for (let j = 0; j < maze.cols(); j++) {
                            let root = UnionFind.find(EllerGenMaze.getVertexAtPos([r, j]));
                            if (!(root.id() in root_map)) {
                                root_map[root.id()] = [];
                            }
                            root_map[root.id()].push(j);
                        }

                        for (let key in root_map) {
                            let set_size = root_map[key].length;
                            /* can also be Math.floor((set_size+1)/2) */
                            for (let l = 0; l < (set_size+1)/2; l++) {
                            //for (let l = 0; l < 1; l++) {
                                let drop = GenMaze.getRandomItem(root_map[key]);
                                let pos = [r, drop];
                                let pos_down = [r+1, drop];
                                let v1 = EllerGenMaze.getVertexAtPos(pos);
                                let v2 = EllerGenMaze.getVertexAtPos(pos_down);
                                UnionFind.union(v1, v2);
                                if (MazeSystem.GUI) {
                                    maze.removeDirWall(pos, new Direction(AuxDirection.down));
                                    maze.setPosWhite(pos_down);
                                }

                            }
                        }
                    }
                    else {

                    }

                    r++;
                    c = 0;

                    if (r === 2) {
                        //terminate = true;
                    }
                }
                steps++;
                if (MazeSystem.GUI) {
                    if (MazeSystem.curgen === MazeSystem.gens.eller)
                        setTimeout(stepIt, MazeSystem.speed);
                }
                else {
                    stepIt();
                }

            }
            else {
                /* exit */
                steps++;
                let timer1 = performance.now();
                let elapsed = ((timer1 - timer0)/1000).toFixed(5);
                MazeSystem.addTimer(steps, elapsed);
            }
        }
        stepIt();
    }
}