/**
 * Created by GentlyGuitar on 3/13/2017.
 */


class MazeSystem {
    static init() {
        MazeSystem.GUI = true;
        MazeSystem.maze = MazeSystem.createMaze();
        MazeSystem.maze.appendToBody();
        MazeSystem.initGenSelector();
        MazeSystem.addSpeedSelector();
        MazeSystem.initSolveSelector();

        //PrimGenMaze.run(MazeSystem.maze);
    }

    static createMaze() {
        let dim;
        if (MazeSystem.GUI) {
            dim = 10;
        }
        else {
            dim = 40; /* kruskal can't handle 50*50 */
        }

        return new Maze(dim, dim);
    }

    static useNewMaze() {
        MazeSystem.removeTimer();
        MazeSystem.maze.remove();
        MazeSystem.maze = MazeSystem.createMaze();
        MazeSystem.maze.prependToBody();
    }


    static addTimer(steps, time) {
        if ($("#timer").length === 0) {
            let para = $("<p>");
            para.html("That took me " + steps + " steps (" + time + " seconds)");
            para.css("text-align", "center");
            para.attr("id", "timer");
            MazeSystem.timer = para;
            $("body").append(para).fadeIn(1000);
        }
    }

    static removeTimer() {
        if (MazeSystem.timer !== undefined) {
            MazeSystem.timer.remove();
        }
    }

    static addSelectorItem(selector, value, text) {
        let op = $("<option>");
        op.html(text);
        op.css("value", value);
        selector.append(op);
    }

    static createSelector(text) {
        let selector = $("<select>");
        selector.css("display", "block"); // vital
        selector.css("margin", "auto");
        selector.css("margin-bottom", "15px");
        selector.css("font-family", "Consolas");

        let def_op = $("<option>");
        def_op.attr("selected", "");
        def_op.attr("disabled", "");
        def_op.html(text);
        selector.append(def_op);
        return selector;
    }

    static addSpeedSelector() {
        let selector = MazeSystem.createSelector("speed");
        MazeSystem._speed_selector = selector;
        $("body").append(MazeSystem._speed_selector);
        MazeSystem.addSelectorItem(selector, 10, 10);
        MazeSystem.addSelectorItem(selector, 50, 50);
        MazeSystem.addSelectorItem(selector, 500, 500);
        MazeSystem.addSelectorItem(selector, 1500, 1500);

        MazeSystem.speed = 50;

        selector.click(function (maze) {
            let value = selector.val();
            MazeSystem.speed = parseInt(value);
            console.log("set speed", MazeSystem.speed);
        });
    }

    static initGenSelector() {
        let selector = MazeSystem.createSelector("generation");
        MazeSystem._gen_selector = selector;
        $("body").append(MazeSystem._gen_selector);

        MazeSystem.gens = {
            ab: "Aldous-Broder",
            backtrack: "Backtracking",
            kruskal: "Kruskal",
            prim: "Prim",
            tree_random: "Growing Tree (random)",
            tree_newest: "Growing Tree (newest)",
            tree_mixed: "Growing Tree (evenly mixed)",
            eller: "Eller",
        };

        for (let key in MazeSystem.gens) {
            MazeSystem.addSelectorItem(selector, key, MazeSystem.gens[key]);
        }

        selector.click(function (maze) {
            let value = selector.val();
            console.log("selected", value);
            MazeSystem.curgen = value;

            switch (value) {
                case MazeSystem.gens.ab: {
                    MazeSystem.useNewMaze();
                    ABGenMaze.run(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.backtrack: {
                    MazeSystem.useNewMaze();
                    BackTrackGenMaze.run(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.kruskal: {
                    MazeSystem.useNewMaze();
                    KruskalGenMaze.run(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.prim: {
                    MazeSystem.useNewMaze();
                    PrimGenMaze.run(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.tree_random: {
                    MazeSystem.useNewMaze();
                    TreeGenMaze.run_random(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.tree_newest: {
                    MazeSystem.useNewMaze();
                    TreeGenMaze.run_newest(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.tree_mixed: {
                    MazeSystem.useNewMaze();
                    TreeGenMaze.run_mixed(MazeSystem.maze);
                    break;
                }
                case MazeSystem.gens.eller: {
                    MazeSystem.useNewMaze();
                    EllerGenMaze.run(MazeSystem.maze);
                    break;
                }
            }

        });
    }

    static initSolveSelector() {
        let selector = MazeSystem.createSelector("solving");
        MazeSystem._solve_selector = selector;
        $("body").append(MazeSystem._solve_selector);

        MazeSystem.solves = {
            dfs: "DFS",
        };

        for (let key in MazeSystem.solves) {
            MazeSystem.addSelectorItem(selector, key, MazeSystem.solves[key]);
        }

        selector.change(function (maze) {
            let value = selector.val();
            console.log("selected", value);
            switch (value) {
                case MazeSystem.solves.dfs: {
                    //MazeSystem.useNewMaze();
                    DFSSolveMaze.run(MazeSystem.maze);
                    break;
                }
                case "xxx": {
                    XXXSolveMaze.run(MazeSystem.maze);
                    break;
                }

            }

        });
    }
}


$(function () {
    MazeSystem.init();
    //PrimGenMaze.run(MazeSystem.maze);
    //BackTrackGenMaze.run(maze);
    //KruskalGenMaze.run(maze);
});
