/**
 * Created by GentlyGuitar on 3/14/2017.
 */

class SolveMaze {

}

class DFSSolveMaze extends SolveMaze {
    static run(maze) {
        DFSSolveMaze.maze = maze;
        DFSSolveMaze.success = false;
        DFSSolveMaze.from_node = null;
        maze.drawWalker();
        maze.drawDestination();
        DFSSolveMaze.path = [maze.walker().pos_str()];
        DFSSolveMaze.dfs(maze.graph(), maze.walker().pos_str(), maze.dest_str(), null);
        DFSSolveMaze.drawPath(maze);
    }

    static drawPath(maze) {
        let len = DFSSolveMaze.path.length;
        let i = 0;
        let terminate = false;
        function stepIt() {
            if (i < len && !terminate) {
                let pos = GenMaze.strToPos(DFSSolveMaze.path[i]);
                DFSSolveMaze.maze.moveWalkerTo(pos);
                if (DFSSolveMaze.path[i] === maze.dest_str()) {
                    terminate = true;
                }
                i++;
                setTimeout(stepIt, 500);
            }
        }
        stepIt();
    }

    // static dfs(g, s, t) {
    //     if (DFSSolveMaze.success) {
    //         return;
    //     }
    //
    //     g.mark(s);
    //
    //     console.log("cur", s);
    //     for (let n of g.getAdjacentList(s)) {
    //         if (n === t) {
    //             DFSSolveMaze.success = true;
    //             setTimeout(function () {
    //                 DFSSolveMaze.maze.moveWalkerTo(GenMaze.strToPos(n));
    //             }, 1000);
    //         }
    //
    //         else if (!g.isMarked(n)) {
    //             setTimeout(function () {
    //                 console.log("to", n);
    //                 DFSSolveMaze.maze.moveWalkerTo(GenMaze.strToPos(n));
    //                 DFSSolveMaze.dfs(g, n, t);
    //             }, 1000);
    //         }
    //     }
    // }

    static dfs(g, s, t, f) {
        if (DFSSolveMaze.success) {
            return;
        }
        g.mark(s);

        console.log("last node", f);

        for (let n of g.getAdjacentList(s)) {
            console.log("n", n);
            if (DFSSolveMaze.success) {
                return;
            }

            if (n === t) {
                DFSSolveMaze.success = true;
                DFSSolveMaze.path.push(n);
                console.log("success");
                return;
            }


            if (n === f) {
                console.log("catch parent");
                continue;
            }

            DFSSolveMaze.path.push(n);

            if (!g.isMarked(n)) {
                DFSSolveMaze.dfs(g, n, t, s);
            }
        }

        DFSSolveMaze.path.push(f);
        //DFSSolveMaze.path.push(GenMaze.strToPos(s));
    }
}